﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Stellar.ServiceBroker.Repository
{
    public class BaseRepository
    {
        private readonly string _connectionString;
        private readonly string _sql;

        public BaseRepository()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["stellar"].ConnectionString;
            _sql = "SELECT id, name FROM dbo.ServiceBroker";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SqlCommand CreateSqlCommand()
        {
            using (var sqlCommand = new SqlCommand(_sql, new SqlConnection(_connectionString)))
            {
                sqlCommand.Connection.Open();
                sqlCommand.CommandText = _sql;
                sqlCommand.CommandType = CommandType.Text;
                return sqlCommand;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void StartSqlDependency()
        {
            SqlDependency.Start(_connectionString);
        }
    }
}
