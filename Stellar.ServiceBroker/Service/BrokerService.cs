﻿using Stellar.ServiceBroker.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stellar.ServiceBroker.Service
{
    public class BrokerService
    {
        private static SqlDependency _sqlDependency;
        private BaseRepository _baseRepository;
        public delegate void OnDataChangeDelegate();
        public event OnDataChangeDelegate OnDataChangeHandler;

        public BrokerService()
        {
            _baseRepository = new BaseRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            Connect();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Connect()
        {
            _baseRepository.StartSqlDependency();
            var sqlCommand = _baseRepository.CreateSqlCommand();
            _sqlDependency = new SqlDependency(sqlCommand);
            _sqlDependency.OnChange += new OnChangeEventHandler(OnChange);
            sqlCommand.ExecuteReader();
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnChange(object sender, SqlNotificationEventArgs e)
        {
            Console.Write("Fire to something");
            Connect();

            if (OnDataChangeHandler != null)
            {
                OnDataChangeHandler.Invoke();
            }
        }
    }
}
