﻿using Stellar.ServiceBroker.Service;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace Stellar.ServiceBroker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BrokerService _brokerService;

        public MainWindow()
        {
            InitializeComponent();
            _brokerService = new BrokerService();
            _brokerService.Start();
            _brokerService.OnDataChangeHandler += new BrokerService.OnDataChangeDelegate(OnDataChange);
        }

        public void OnDataChange()
        {
            Console.Write("This came from broker service");
        }
    }
}
