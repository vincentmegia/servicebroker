﻿Stellar Copyright 2014


This is a basic example of how SQL SERVER 2008 Service broker can be used to listen 
if any changes is happening to database.

How to work
	1) For this example to work please execute the database script located in Database directory or folder,
		this will contain script to create a table
	2) Execute command below to enable the service broker,
		ALTER DATABASE <your database> SET ENABLE_BROKER
	3) Change the database connection string in the app.config
	4) Run the project

System requirement
	1) Sql Server 2008
	2) Visual Studio